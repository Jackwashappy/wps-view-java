package com.web.wps.util.upload;

import org.springframework.web.multipart.MultipartFile;

public interface FileInterface {

    ResFileDTO uploadMultipartFile(MultipartFile file);

    void deleteFile(String key);

}
