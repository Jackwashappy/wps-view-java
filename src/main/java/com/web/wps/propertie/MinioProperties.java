package com.web.wps.propertie;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Data
@Component
@Configuration
@ConfigurationProperties(prefix = "minio")
public class MinioProperties {

    private String fileUrlPrefix;
    private String bucketName;
    private String diskName;
    private String regionId;
    private String endpoint;
    private String accessKey;
    private String accessSecret;

    /**
     * 注入minio 客户端
     */
    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(endpoint)
                .credentials(accessKey, accessSecret)
                .build();
    }

}
